<?php

/**
 * @file
 * More support for views.
 */


/**
 * Implements hook_field_views_data_views_data_alter().
 */
function civicrm_reference_fields_field_views_data_views_data_alter(&$data, $field) {
  $civicrm_table = NULL;
  if ($field['type'] == 'civicrm_reference_fields_contribution') {
    $civicrm_table = 'civicrm_contribution_page';
    $id_field = 'id';
    $label = t('contribution page id');
  }
  elseif ($field['type'] == 'civicrm_reference_fields_event') {
    $civicrm_table = 'civicrm_event';
    $id_field = 'id';
    $label = t('event id');
  }
  elseif ($field['type'] == 'civicrm_reference_fields_pcp') {
    $civicrm_table = 'civicrm_pcp';
    $id_field = 'id';
    $label = t('pcp id');
  }

  // Different sort of civicrm reference field, return.
  if (empty($civicrm_table)) {
    return;
  }

  $field_name = $field['field_name'];
  $data_key = 'field_data_' . $field_name;
  $revision_key = 'field_revision_' . $field_name;
  $field_id = $field_name . '_civicrm_reference_id';

  // Field data table.
  $data[$data_key]['table']['join'][$civicrm_table] = array(
   'left_field' => $id_field,
    'field' => $field_name,
  );
  $data[$data_key][$field_id]['relationship'] = array(
    'handler' => 'views_handler_relationship',
    'base' => $civicrm_table,
    'base field' => $id_field,
    'label' => $label,
  );

  // Field revision table.
  $data[$revision_key]['table']['join'][$civicrm_table] = array(
    'left_field' => $id_field,
    'field' => $field_name,
  );
  $data[$revision_key][$field_id]['relationship'] = array(
    'handler' => 'views_handler_relationship',
    'base' => $civicrm_table,
    'base field' => $id_field,
    'label' => $label,
  );
}
